package game

import (
	"log"
	"math/rand"
)

var _ = log.Println

type Cell struct {
	Energy byte
	Owner  PlayerId
	Action PlayerAction
}

func (c *Cell) Owned() bool {
	return c.Owner > 0
}

func (c *Cell) Moves() bool {
	return c.Action.IsMove()
}

func (c *Cell) Punish() {
	if c.Energy > 0 {
		c.Energy -= 1
		if c.Energy == 0 {
			c.Owner = 0
			c.Action = PlayerActionNone
		}
	}
}

type Map struct {
	W, H        int
	Cell        [][]Cell
	EnergyLimit byte
}

func NewMap(width int, height int, life_density float64, energy_limit byte) *Map {
	r := make([][]Cell, height)
	for y := 0; y < height; y++ {
		r[y] = make([]Cell, width)
		for x := 0; x < width; x++ {
			if rand.Float64() < life_density {
				r[y][x].Energy = byte(1 + rand.Intn(int(energy_limit)))
			}
		}
	}
	return &Map{width, height, r, energy_limit}
}

func (m *Map) normalize(x, y int) (int, int) {
	switch {
	case x < 0:
		x += m.W
	case x >= m.W:
		x -= m.W
	}

	switch {
	case y < 0:
		y += m.H
	case y >= m.H:
		y -= m.H
	}

	return x, y
}

func (m *Map) At(x, y int) (*Cell, int, int) {
	x, y = m.normalize(x, y)
	return &m.Cell[y][x], x, y
}

func (m *Map) prepare(player map[PlayerId]*Player) {
	for y := 0; y < m.H; y++ {
		for x := 0; x < m.W; x++ {
			c := &m.Cell[y][x]
			if c.Owner != 0 {
				p := player[c.Owner] // fatal if not found, shouldn't happen
				c.Action = p.Action
			}
		}
	}
}

type CandidateList []*Cell

/*
var directions = [2]int{-1, 1}
func (m *Map) candidatesTo(x, y int) CandidateList {
	candidates := make(CandidateList, 0, 4)

	for dy := range directions {
		for dx := range directions {
			x2, y2 := x+dx, y+dy
			cc, _, _ := m.At(x2, y2)
			if cc.Owned() && cc.Moves() {
				ex, ey := cc.Action.MoveVector()
				x2, y2 = m.normalize(x2+ex, y2+ey)
				if x2 == x && y2 == y {
					candidates = append(candidates, cc)
				}
			}
		}
	}

	return candidates
}
*/
func (m *Map) candidatesTo(x, y int) CandidateList {
	candidates := make(CandidateList, 0, 4)

	c, _, _ := m.At(x-1, y)
	if c.Owned() && c.Action == PlayerActionRight {
		candidates = append(candidates, c)
	}

	c, _, _ = m.At(x+1, y)
	if c.Owned() && c.Action == PlayerActionLeft {
		candidates = append(candidates, c)
	}

	c, _, _ = m.At(x, y-1)
	if c.Owned() && c.Action == PlayerActionDown {
		candidates = append(candidates, c)
	}

	c, _, _ = m.At(x, y+1)
	if c.Owned() && c.Action == PlayerActionUp {
		candidates = append(candidates, c)
	}

	return candidates
}

func (c *Cell) consume(d *Cell, m *Map) {
	if d.Energy == 0 {

		*d = *c
		*c = Cell{}

	} else {

		d.Owner = c.Owner

		fit := m.EnergyLimit - c.Energy

		if d.Energy <= fit { // absorb
			d.Energy += c.Energy
			c.Energy = 0
			c.Owner = 0
		} else { // adjoin
			c.Energy = m.EnergyLimit
			d.Energy -= fit
		}
	}
}

func (c CandidateList) reset() {
	for _, ch := range c {
		ch.Action = PlayerActionNone
	}
}

func (c CandidateList) best() *Cell {

	best, collision := c[0], false // can't fail, there's at least one
	for _, ch := range c[1:] {
		switch {
		case ch.Energy > best.Energy:
			best = ch
			collision = false
		case ch.Energy == best.Energy:
			collision = true
		}
	}

	if !collision {
		return best
	} else {
		return nil
	}
}

func (m *Map) resolve_takeover(x int, y int, c *Cell) bool {

	//log.Println(" - resolve_takeover at ", x, ",", y)
	candidates := m.candidatesTo(x, y)
	//log.Println(" - candidates ", len(candidates))
	// takeover resolution is always successful
	candidates.reset()

	best := candidates.best()

	if best != nil {
		if c.Energy < best.Energy {
			best.consume(c, m)
		}
		for _, ch := range candidates {
			if ch != best {
				ch.Punish()
			}
		}
	} else {
		for _, ch := range candidates {
			ch.Punish()
		}
	}

	return true
}

/* x,y is being attacked
 * preconditions: is there a cell in x,y and at least one attacker
 *
 * Algorithm:
 * if c@x,y is moving away from attack, postpone
 * else, either it is static or attacking. resolve
 */
func (m *Map) resolve_attack(x int, y int, victim *Cell) bool {

	//log.Println(" - resolve_attack at ", x, ",", y)
	attackers := m.candidatesTo(x, y)
	var fightback *Cell = nil // who was the victim fighting back (if any)

	if victim.Moves() {
		dx, dy := victim.Action.MoveVector()
		fightback, _, _ = m.At(x+dx, y+dy)
		found := false
		for _, ch := range attackers {
			if ch == fightback {
				found = true
				break
			}
		}

		// can't solve because victim could move away
		if !found {
			//log.Println(" - postponed")
			return false
		}

	} else {
		// failed move of convoy, don't attack self
		if len(attackers) == 1 && attackers[0].Owner == victim.Owner {
			attackers = append(attackers, victim)
			attackers.reset()
			return true
		}
	}

	attackers = append(attackers, victim)
	best := attackers.best()

	if best != nil {

		for _, ch := range attackers {
			if ch != best {
				ch.Punish()
			}
		}

		if best != victim {
			best.consume(victim, m)
		} else {
			if fightback != nil {
				best.consume(fightback, m)
			}
		}

	} else {

		for _, ch := range attackers {
			ch.Punish()
		}
	}

	attackers.reset()
	return true
}

func (m *Map) resolve(x int, y int) bool {
	c, x, y := m.At(x, y)
	if c.Owned() {
		return m.resolve_attack(x, y, c)
	}
	return m.resolve_takeover(x, y, c)
}

func (m *Map) resolve_move(x int, y int, c *Cell) bool {
	//log.Println("resolve_move at ", x, ",", y)
	dx, dy := c.Action.MoveVector()
	return m.resolve(x+dx, y+dy)
}

func (m *Map) update() bool {

	completed := true

	/* this is a very simple logic because it will try to resolve the same cell
	 * more than once
	 */
	for y := 0; y < m.H; y++ {
		for x := 0; x < m.W; x++ {
			c := &m.Cell[y][x]
			if c.Owned() && c.Moves() {
				if !m.resolve_move(x, y, c) {
					completed = false
				}
			}
		}
	}

	return completed
}

func (m *Map) Update(player map[PlayerId]*Player) int {

	m.prepare(player)
	iter := 1
	for ; m.update() == false; iter++ {
	}
	m.CleanupPlayers(player)
	return iter
}
