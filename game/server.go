package game

import (
	"github.com/gorilla/websocket"
	"log"
	"net/http"
)

type Server struct {
	Address string

	handler  ConnectionHandler
	upgrader websocket.Upgrader
}

type ConnectionHandler interface {
	HandleConnection(*websocket.Conn)
}

func NewServer(address string, client_data_path string) *Server {

	server := new(Server)
	server.Address = address

	http.Handle("/", http.FileServer(http.Dir(client_data_path)))
	http.Handle("/life/connect", server)

	return server
}

func (this *Server) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	socket, err := this.upgrader.Upgrade(w, r, nil)
	if err != nil {
		log.Println("upgrade connection to websocket failed: ", err)
		return
	}

	log.Println("New connection from ", socket.RemoteAddr().String())
	this.handler.HandleConnection(socket)
}

func (this *Server) Launch() error {
	return http.ListenAndServe(this.Address, nil)
}

func (this *Server) SetHandler(h ConnectionHandler) {
	this.handler = h
}
