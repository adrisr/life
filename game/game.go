package game

import (
	"github.com/gorilla/websocket"
	"log"
	"time"
)

type Game struct {
	Width, Height int
	Density       float64
	EnergyLimit   byte

	Hz int

	MaxPlayers   PlayerId
	nextPlayerId PlayerId

	player map[PlayerId]*Player

	PlayerConnected chan *websocket.Conn
	PlayerActions   chan *PlayerActionMessage

	world *Map

	clock *time.Ticker
}

func New() *Game {
	g := new(Game)

	g.Width = 128
	g.Height = 128
	g.Density = 0.1
	g.EnergyLimit = 16
	g.MaxPlayers = 100

	g.Hz = 4

	g.player = make(map[PlayerId]*Player)

	g.PlayerConnected = make(chan *websocket.Conn)
	g.PlayerActions = make(chan *PlayerActionMessage)
	return g
}

func (this *Game) Stop() {
	this.nextPlayerId = 0
	this.clock.Stop()
}

func (g *Game) Start() {
	g.world = NewMap(g.Width, g.Height, g.Density, g.EnergyLimit)
	g.clock = time.NewTicker(time.Second / time.Duration(g.Hz))

	go g.dispatcher()
}

func (this *Game) HandleConnection(client *websocket.Conn) {
	this.PlayerConnected <- client
}

func (this *Game) onPlayerConnected(sock *websocket.Conn) {
	if len(this.player) < int(this.MaxPlayers) {
		this.nextPlayerId++
		player := NewPlayer(this.nextPlayerId, sock, this)
		this.player[player.Id] = player
	} else {
		sock.Close()
	}
}

func (this *Game) onPlayerAction(msg *PlayerActionMessage) {
	player, ok := this.player[msg.Id]
	if !ok {
		log.Println("Received message from unknown player ", msg.Id)
		return
	}

	player.Action = msg.Action
}

func (this *Game) dispatcher() {

	for {
		select {
		case sock := <-this.PlayerConnected:
			this.onPlayerConnected(sock)

		case msg := <-this.PlayerActions:
			this.onPlayerAction(msg)

		case now := <-this.clock.C:
			this.update(now)
		}
	}
}

func (this *Game) update(now time.Time) {

	// update every cell
	start := time.Now()
	this.world.Update(this.player)
	took := time.Now().Sub(start)

	log.Println("Update took ", took)
}
