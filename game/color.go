package game

import (
	"log"
)

type ColorMap struct {
	Cell  [][]int
	Color int
	M     *Map
}

func (m *Map) NewColorMap() *ColorMap {

	cmap := make([][]int, m.H)
	for y := 0; y < m.H; y++ {
		cmap[y] = make([]int, m.W)
	}

	return &ColorMap{Cell: cmap, Color: 1, M: m}
}

type Point struct {
	X, Y int
}

type PlayerRegion struct {
	Color int
	Score uint32
}

type PaintList []Point

var Directions = [...]Point{{0, -1}, {0, 1}, {-1, 0}, {1, 0}}

func (cmap *ColorMap) paintInDirection(x int, y int, incX int, incY int, target PlayerId) (uint32, PaintList) {

	score := uint32(0)
	pending := make(PaintList, 0)

	for {
		var c *Cell
		c, x, y = cmap.M.At(x+incX, y+incY)
		current := cmap.Cell[y][x]
		if c.Owner == target && current == 0 {
			cmap.Cell[y][x] = cmap.Color
			score += uint32(c.Energy)

			for _, check := range Directions {
				c, cx, cy := cmap.M.At(x+check.X, y+check.Y)
				if c.Owner == target && cmap.Cell[cy][cx] == 0 {
					pending = append(pending, Point{X: cx, Y: cy})
				}
			}

		} else {
			if c.Owner == target && current != cmap.Color {
				log.Panic("Painting sux")
			}
			break
		}
	}

	return score, pending
}

func (cmap *ColorMap) paintNeighbours(x int, y int, target PlayerId) (uint32, PaintList) {

	if cmap.Cell[y][x] == 0 {
		cmap.Cell[y][x] = cmap.Color
		score := uint32(cmap.M.Cell[y][x].Energy)
		pending := make(PaintList, 0)

		for _, dir := range Directions {
			num, list := cmap.paintInDirection(x, y, dir.X, dir.Y, target)
			score += num
			pending = append(pending, list...)
		}

		return score, pending
	}

	return 0, PaintList{}
}

func (cmap *ColorMap) paint(baseX int, baseY int) *PlayerRegion {
	c, x, y := cmap.M.At(baseX, baseY)
	if !c.Owned() {
		log.Panic("Painting empty region")
	}

	target := c.Owner

	score, pending := cmap.paintNeighbours(x, y, target)

	for len(pending) > 0 {
		next_list := make(PaintList, 0)

		for _, p := range pending {
			c, list := cmap.paintNeighbours(p.X, p.Y, target)
			score += c
			next_list = append(next_list, list...)
		}

		pending = next_list
	}

	color := cmap.Color
	cmap.Color++
	return &PlayerRegion{Color: color, Score: score}
}

func (cmap *ColorMap) kill(color int) {

	W, H := cmap.M.W, cmap.M.H

	for y := 0; y < H; y++ {
		for x := 0; x < W; x++ {
			if cmap.Cell[y][x] == color {
				cmap.M.Cell[y][x].Owner = 0
			}
		}
	}
}

func (cmap *ColorMap) printColor(name string, color int, owner PlayerId) {

	s := make([]byte, cmap.M.W)

	log.Println(name)

	code := byte('A' + int(owner) - 1)

	for y := 0; y < cmap.M.H; y++ {
		for x := 0; x < cmap.M.W; x++ {
			if cmap.Cell[y][x] == color {
				s[x] = code
			} else {
				s[x] = '.'
			}
		}
		log.Println(string(s))
	}
}

func (m *Map) CleanupPlayers(player map[PlayerId]*Player) {

	W, H := m.W, m.H
	cmap := m.NewColorMap()

	info := make(map[PlayerId]*PlayerRegion)

	for y := 0; y < H; y++ {
		for x := 0; x < W; x++ {
			if cmap.Cell[y][x] == 0 {
				owner := m.Cell[y][x].Owner
				if owner != 0 {
					region := cmap.paint(x, y)

					existing, found := info[owner]

					if !found {
						//cmap.printColor("saving", region.Color, owner)
						info[owner] = region
					} else {
						if existing.Score < region.Score {
							info[owner] = region
							//cmap.printColor("removing", existing.Color, owner)
							//cmap.printColor("keeping", region.Color, owner)
							cmap.kill(existing.Color)
						} else {
							//cmap.printColor("removing", region.Color, owner)
							//cmap.printColor("keeping", existing.Color, owner)
							cmap.kill(region.Color)
						}
					}
				}
			}
		}
	}

	for pid, p := range player {
		inf, found := info[pid]
		if found {
			p.Score = inf.Score
		} else {
			p.Score = 0 // hahah ur dead!
		}
	}
}
