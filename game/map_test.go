package game

import (
	"log"
	"math/rand"
	"testing"
	"time"
)

var _ = rand.Int()

func (origin *Map) checkUpdate(player map[PlayerId]*Player, result *Map) bool {

	if result == origin {
		log.Println("Passing same pointer to check!")
		return false
	}

	start := time.Now()
	origin.Update(player)
	log.Println("Update took ", time.Now().Sub(start))

	if origin.W != result.W {
		log.Printf("Different width: %d vs %d\n", origin.W, result.W)
		return false
	}

	if origin.H != result.H {
		log.Printf("Different height: %d vs %d\n", origin.H, result.H)
		return false
	}

	if origin.EnergyLimit != result.EnergyLimit {
		log.Println("Different energy limit: %d vs %d\n", origin.EnergyLimit, result.EnergyLimit)
		return false
	}

	for y := 0; y < origin.H; y++ {
		for x := 0; x < origin.W; x++ {
			o, _, _ := origin.At(x, y)
			r, _, _ := result.At(x, y)
			if *o != *r {
				log.Printf("At %d,%d: Got %+v want %+v\n", x, y, *o, *r)
				return false
			}
		}
	}

	return true
}

func (m *Map) Copy() *Map {
	r := NewMap(m.W, m.H, 0.0, m.EnergyLimit)

	for y := 0; y < m.H; y++ {
		for x := 0; x < m.W; x++ {
			r.Cell[y][x] = m.Cell[y][x]
		}
	}
	return r
}

func TestAttack1(t *testing.T) {
	m := NewMap(4, 4, 0, 8)
	p := map[PlayerId]*Player{
		1: &Player{Id: 1},
		2: &Player{Id: 2},
	}

	p[1].Action = PlayerActionRight
	p[2].Action = PlayerActionLeft
	m.Cell[1][1] = Cell{Energy: 4, Owner: 1}
	m.Cell[1][2] = Cell{Energy: 4, Owner: 2}

	result := m.Copy()
	result.Cell[1][1].Energy, result.Cell[1][2].Energy = 3, 3

	if !m.checkUpdate(p, result) {
		t.Error("Test not passed")
	}
}

func TestMoveSingle(t *testing.T) {
	m := NewMap(4, 4, 0, 8)
	p := map[PlayerId]*Player{
		1: &Player{Id: 1},
	}

	p[1].Action = PlayerActionRight
	m.Cell[1][1] = Cell{Energy: 4, Owner: 1}

	result := m.Copy()
	result.Cell[1][2] = result.Cell[1][1]
	result.Cell[1][1] = result.Cell[0][0]

	if !m.checkUpdate(p, result) {
		t.Error("Test not passed")
	}
}

func TestMoveRow(t *testing.T) {
	m := NewMap(4, 4, 0, 8)
	p := map[PlayerId]*Player{
		1: &Player{Id: 1},
	}

	p[1].Action = PlayerActionRight
	m.Cell[1][0] = Cell{Energy: 4, Owner: 1}
	m.Cell[1][1] = Cell{Energy: 4, Owner: 1}
	m.Cell[1][2] = Cell{Energy: 4, Owner: 1}

	result := m.Copy()
	result.Cell[1][3] = result.Cell[1][2]
	result.Cell[1][0] = result.Cell[0][0]

	if !m.checkUpdate(p, result) {
		t.Error("Test not passed")
	}
}

func TestConsume1(t *testing.T) {
	m := NewMap(4, 4, 0, 8)
	p := map[PlayerId]*Player{
		1: &Player{Id: 1},
	}

	p[1].Action = PlayerActionRight
	m.Cell[1][1] = Cell{Energy: 4, Owner: 1}
	m.Cell[1][2] = Cell{Energy: 1}

	result := m.Copy()
	result.Cell[1][2] = result.Cell[1][1]
	result.Cell[1][2].Energy = 5
	result.Cell[1][1] = Cell{}

	if !m.checkUpdate(p, result) {
		t.Error("Test not passed")
	}
}

func TestConsume2Collision(t *testing.T) {
	m := NewMap(4, 4, 0, 8)
	p := map[PlayerId]*Player{
		1: &Player{Id: 1, Action: PlayerActionRight},
	}

	m.Cell[1][1] = Cell{Energy: 4, Owner: 1}
	m.Cell[1][2] = Cell{Energy: 4}

	result := m.Copy()

	if !m.checkUpdate(p, result) {
		t.Error("Test not passed")
	}
}

func TestConsume3(t *testing.T) {
	m := NewMap(4, 4, 0, 8)
	p := map[PlayerId]*Player{
		1: &Player{Id: 1, Action: PlayerActionRight},
	}

	m.Cell[1][1] = Cell{Energy: 5, Owner: 1}
	m.Cell[1][2] = Cell{Energy: 3}

	result := m.Copy()
	result.Cell[1][2] = result.Cell[1][1]
	result.Cell[1][2].Energy = 8
	result.Cell[1][1] = Cell{}

	if !m.checkUpdate(p, result) {
		t.Error("Test not passed")
	}
}

func TestConsume4(t *testing.T) {
	m := NewMap(4, 4, 0, 8)
	p := map[PlayerId]*Player{
		1: &Player{Id: 1, Action: PlayerActionRight},
	}

	m.Cell[1][1] = Cell{Energy: 5, Owner: 1}
	m.Cell[1][2] = Cell{Energy: 4}

	result := m.Copy()
	result.Cell[1][1].Energy = 8
	result.Cell[1][2].Energy = 1
	result.Cell[1][2].Owner = 1

	if !m.checkUpdate(p, result) {
		t.Error("Test not passed")
	}
}

func TestWrap1(t *testing.T) {
	m := NewMap(4, 4, 0, 8)
	p := map[PlayerId]*Player{
		1: &Player{Id: 1, Action: PlayerActionUp},
	}

	cell, empty := Cell{Energy: 5, Owner: 1}, Cell{}

	m.Cell[0][0] = cell

	result := m.Copy()
	result.Cell[0][0] = empty
	result.Cell[3][0] = cell

	if !m.checkUpdate(p, result) {
		t.Error("Wrap up not passed")
	}

	p[1].Action = PlayerActionDown
	result.Cell[0][0] = cell
	result.Cell[3][0] = empty

	if !m.checkUpdate(p, result) {
		t.Error("Wrap down not passed")
	}

	p[1].Action = PlayerActionLeft
	result.Cell[0][0] = empty
	result.Cell[0][3] = cell

	if !m.checkUpdate(p, result) {
		t.Error("Wrap left not passed")
	}

	p[1].Action = PlayerActionRight
	result.Cell[0][0] = cell
	result.Cell[0][3] = empty

	if !m.checkUpdate(p, result) {
		t.Error("Wrap right not passed")
	}

}

func TestAttackLarge1(t *testing.T) {
	/*   0 1 2 3 4 5 6 7
	-- 0 . . . . . . . .
	-- 1 . . . . . . . .
	-- 2 . A x C . . . .
	-- 3 A A A . B B B B
	-- 4 . A . . . B . .
	-- 5 . . . . . . . .
	-- 6 . . . . . . . .
	-- 7 . . . . . . . .
	*/

	m := NewMap(8, 8, 0, 8)
	p := map[PlayerId]*Player{
		1: &Player{Id: 1, Action: PlayerActionRight},
		2: &Player{Id: 2, Action: PlayerActionLeft},
		3: &Player{Id: 3, Action: PlayerActionLeft},
	}

	empty := Cell{}
	cellA := Cell{Energy: 5, Owner: 1}
	cellB := Cell{Energy: 2, Owner: 2}
	cellC := Cell{Energy: 6, Owner: 3}

	m.Cell[2][1] = cellA
	m.Cell[2][2] = Cell{Energy: 1}
	m.Cell[2][3] = cellC
	m.Cell[3][0] = cellA
	m.Cell[3][1] = cellA
	m.Cell[3][2] = cellA
	m.Cell[3][4] = cellB
	m.Cell[3][5] = cellB
	m.Cell[3][6] = cellB
	m.Cell[3][7] = cellB
	m.Cell[4][5] = cellB

	result := m.Copy()
	/*   0 1 2 3 4 5 6 7
	-- 0 . . . . . . . .
	-- 1 . . . . . . . .
	-- 2 . A'C'. . . . .
	-- 3 . A A A B'B B B
	-- 4 . . A . B . . .
	-- 5 . . . . . . . .
	-- 6 . . . . . . . .
	-- 7 . . . . . . . .
	*/
	result.Cell[2][1].Energy -= 1
	result.Cell[2][2] = Cell{Energy: 7, Owner: 3}
	result.Cell[2][3] = empty
	result.Cell[3][0] = empty
	result.Cell[3][3] = cellA
	result.Cell[3][4] = Cell{Energy: 1, Owner: 2} //empty
	result.Cell[4][4] = cellB
	result.Cell[4][5] = empty

	if !m.checkUpdate(p, result) {
		t.Error("Wrap up not passed")
	}

}

func TestAttackLarge2(t *testing.T) {
	/*   0 1 2 3 4 5 6 7
	-- 0 . . . . . . . .
	-- 1 . . . . . . . .
	-- 2 . A x C . . . .
	-- 3 A A A . B B B B
	-- 4 . A . . . B . .
	-- 5 . . . . . . . .
	-- 6 . . . . . . . .
	-- 7 . . . . . . . .
	*/

	m := NewMap(8, 8, 0, 8)
	p := map[PlayerId]*Player{
		1: &Player{Id: 1, Action: PlayerActionRight},
		2: &Player{Id: 2, Action: PlayerActionLeft},
		3: &Player{Id: 3, Action: PlayerActionLeft},
	}

	empty := Cell{}
	cellA := Cell{Energy: 5, Owner: 1}
	cellB := Cell{Energy: 1, Owner: 2}
	cellC := Cell{Energy: 6, Owner: 3}

	m.Cell[2][1] = cellA
	m.Cell[2][2] = Cell{Energy: 1}
	m.Cell[2][3] = cellC
	m.Cell[3][0] = cellA
	m.Cell[3][1] = cellA
	m.Cell[3][2] = cellA
	m.Cell[3][4] = cellB
	m.Cell[3][5] = cellB
	m.Cell[3][6] = cellB
	m.Cell[3][7] = cellB
	m.Cell[4][5] = cellB

	result := m.Copy()
	/*   0 1 2 3 4 5 6 7
	-- 0 . . . . . . . .
	-- 1 . . . . . . . .
	-- 2 . A'C'. . . . .
	-- 3 . A A A B B B .
	-- 4 . . A . B . . .
	-- 5 . . . . . . . .
	-- 6 . . . . . . . .
	-- 7 . . . . . . . .
	*/
	result.Cell[2][1].Energy -= 1
	result.Cell[2][2] = Cell{Energy: 7, Owner: 3}
	result.Cell[2][3] = empty
	result.Cell[3][0] = empty
	result.Cell[3][3] = cellA
	result.Cell[3][7] = empty
	result.Cell[4][4] = cellB
	result.Cell[4][5] = empty

	if !m.checkUpdate(p, result) {
		t.Error("Wrap up not passed")
	}

}

func TestAttackLarge3(t *testing.T) {
	/*   0 1 2 3 4 5 6 7
	-- 0 . . . . . . . .
	-- 1 . . . . . . . .
	-- 2 . A x C . . . .
	-- 3 A A A . B B B B
	-- 4 . A . . B . . .
	-- 5 . . . . . . . .
	-- 6 . . . . . . . .
	-- 7 . . . . . . . .
	*/

	m := NewMap(8, 8, 0, 8)
	p := map[PlayerId]*Player{
		1: &Player{Id: 1, Action: PlayerActionRight},
		2: &Player{Id: 2, Action: PlayerActionLeft},
		3: &Player{Id: 3, Action: PlayerActionLeft},
	}

	empty := Cell{}
	cellA := Cell{Energy: 5, Owner: 1}
	cellB := Cell{Energy: 1, Owner: 2}
	cellC := Cell{Energy: 6, Owner: 3}

	m.Cell[2][1] = cellA
	m.Cell[2][2] = Cell{Energy: 1}
	m.Cell[2][3] = cellC
	m.Cell[3][0] = cellA
	m.Cell[3][1] = cellA
	m.Cell[3][2] = cellA
	m.Cell[3][4] = cellB
	m.Cell[3][5] = cellB
	m.Cell[3][6] = cellB
	m.Cell[3][7] = cellB
	m.Cell[4][4] = cellB

	result := m.Copy()
	/*   0 1 2 3 4 5 6 7
	-- 0 . . . . . . . .
	-- 1 . . . . . . . .
	-- 2 . A'C'. . . . .
	-- 3 . A A A B B B .
	-- 4 . . A x . . . .
	-- 5 . . . . . . . .
	-- 6 . . . . . . . .
	-- 7 . . . . . . . .
	*/
	result.Cell[2][1].Energy -= 1
	result.Cell[2][2] = Cell{Energy: 7, Owner: 3}
	result.Cell[2][3] = empty
	result.Cell[3][0] = empty
	result.Cell[3][3] = cellA
	result.Cell[3][7] = empty
	result.Cell[4][3] = Cell{Energy: 1}
	result.Cell[4][4] = empty

	if !m.checkUpdate(p, result) {
		t.Error("Wrap up not passed")
	}
}

func TestDeath1(t *testing.T) {
	/*   0 1 2 3 4 5 6 7
	-- 0 C . . . B . . .
	-- 1 C A . . . B A .
	-- 2 . . A . C . A .
	-- 3 . A A A . . A .
	-- 4 . . B . . . . .
	-- 5 . C . B B . B B
	-- 6 C . C . B . B B
	-- 7 C . . . B . C C
	*/

	m := NewMap(8, 8, 0, 8)

	p := map[PlayerId]*Player{
		1: &Player{Id: 1, Action: PlayerActionNone},
		2: &Player{Id: 2, Action: PlayerActionNone},
		3: &Player{Id: 3, Action: PlayerActionNone},
	}

	cellA := Cell{Energy: 5, Owner: 1}
	cellB := Cell{Energy: 1, Owner: 2}
	cellC := Cell{Energy: 6, Owner: 3}

	for _, p := range [...][2]int{{1, 1}, {1, 6}, {2, 2}, {2, 6}, {3, 1}, {3, 2}, {3, 3}, {3, 6}} {
		m.Cell[p[0]][p[1]] = cellA
	}

	for _, p := range [...][2]int{{0, 4}, {1, 5}, {4, 2}, {5, 3}, {5, 4}, {5, 6}, {5, 7}, {6, 4}, {6, 6}, {6, 7}, {7, 4}} {
		m.Cell[p[0]][p[1]] = cellB
	}

	for _, p := range [...][2]int{{0, 0}, {1, 0}, {2, 4}, {5, 1}, {6, 0}, {6, 2}, {7, 6}, {7, 7}, {7, 0}} {
		m.Cell[p[0]][p[1]] = cellC
	}

	result := m.Copy()

	/*   0 1 2 3 4 5 6 7
	-- 0 C . . . B . . .
	-- 1 C a . . . b a .
	-- 2 . . A . c . a .
	-- 3 . A A A . . a .
	-- 4 . . b . . . . .
	-- 5 . c . B B . b b
	-- 6 C . c . B . b b
	-- 7 C . . . B . C C
	*/
	for _, p := range [...][2]int{{1, 1}, {1, 5}, {1, 6}, {2, 4}, {2, 6}, {3, 6}, {4, 2}, {5, 1}, {5, 6}, {5, 7}, {6, 2}, {6, 6}, {6, 7}} {
		result.Cell[p[0]][p[1]].Owner = 0
	}

	if !m.checkUpdate(p, result) {
		t.Error("Death test not passed")
	}

	for i, e := range [...]uint32{4 * uint32(cellA.Energy), 5 * uint32(cellB.Energy), 6 * uint32(cellC.Energy)} {
		s := p[PlayerId(i+1)].Score
		if s != e {
			t.Error("Bad player ", i+1, " score. Got ", s, " want ", e)
		}
	}
}

/*
func TestRandom(t *testing.T) {

	D := 512
	m := NewMap(D, D, 0.1, 8)

	p := make(map[PlayerId]*Player)

	for i := 0; i < 100; i++ {
		id := PlayerId(i)
		p[id] = &Player{Id: id}
		for {
			x, y := rand.Intn(D), rand.Intn(D)
			if m.Cell[y][x].Owner == 0 {
				m.Cell[y][x].Owner = id
				m.Cell[y][x].Energy = byte(1 + rand.Intn(3))
				break
			}
		}
	}

	for len(p) != 0 {
		for id := range p {
			p[id].Action = PlayerAction(2 + rand.Intn(4))
		}

		start := time.Now()
		m.Update(p)
		log.Println("Update took ", time.Now().Sub(start))

		for id := range p {
			if p[id].Score > 0 {
				log.Println("Player ", id, " score is ", p[id].Score)
			} else {
				log.Println("Player ", id, " died!")
				delete(p, id)
			}
		}
	}
}
*/
