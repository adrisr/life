package game

import (
	"github.com/gorilla/websocket"
	"log"
	"time"
)

type PlayerSharedData struct {
	Next_code int
}

type PlayerState byte

type PlayerId int

type Player struct {
	Id           PlayerId
	X, Y, Size   int
	WriteRequest chan []byte
	LastActivity time.Time

	Action PlayerAction

	Score  uint32
	parent *Game
}

type PlayerAction byte

const (
	PlayerActionNone PlayerAction = iota
	PlayerActionDie

	// move actions are last (see IsMove)
	PlayerActionUp
	PlayerActionDown
	PlayerActionLeft
	PlayerActionRight
)

type PlayerActionMessage struct {
	Id     PlayerId
	Action PlayerAction
}

func (action PlayerAction) IsMove() bool {
	return action >= PlayerActionUp
}

func (action PlayerAction) MoveVector() (x, y int) {
	switch action {
	case PlayerActionUp:
		return 0, -1
	case PlayerActionDown:
		return 0, 1
	case PlayerActionLeft:
		return -1, 0
	case PlayerActionRight:
		return 1, 0
	}

	log.Panic("Bad action for MoveVector(): ", action)
	return 0, 0
}

func NewPlayer(id PlayerId, socket *websocket.Conn, g *Game) *Player {

	player := &Player{
		Id:           id,
		parent:       g,
		LastActivity: time.Now(),
	}

	player.WriteRequest = make(chan []byte)
	go player.reader(socket)
	go player.writer(socket)

	return player
}

var validActions = map[byte]PlayerAction{
	'U': PlayerActionUp,
	'u': PlayerActionUp,
	'D': PlayerActionDown,
	'd': PlayerActionDown,
	'L': PlayerActionLeft,
	'l': PlayerActionLeft,
	'R': PlayerActionRight,
	'r': PlayerActionRight,
}

func (player *Player) Terminate() {
	close(player.WriteRequest)
}

func (player *Player) reader(sock *websocket.Conn) {

	for {
		mtype, data, err := sock.ReadMessage()
		if err != nil {
			break
		}

		action := PlayerActionDie

		if mtype == websocket.TextMessage {
			if len(data) == 1 {
				act, valid := validActions[data[0]]
				if valid {
					action = act
				}
			}
		}

		player.parent.PlayerActions <- &PlayerActionMessage{player.Id, action}
	}

	sock.Close()
	player.Terminate()
}

func (player *Player) writer(sock *websocket.Conn) {
	for message := range player.WriteRequest {
		if err := sock.WriteMessage(websocket.TextMessage, message); err != nil {
			break
		}
	}
	sock.Close()
	player.Terminate()
}
