package main

import (
	"life/game"
	"log"
)

func main() {
	life := game.New()

	life.Start()

	server := game.NewServer("localhost:7777", "client/")
	server.SetHandler(life)

	log.Println("Waiting for connections ...")

	if err := server.Launch(); err != nil {
		log.Fatal("Listen failed on ", server.Address)
	}
}
