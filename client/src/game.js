"use strict";
Life.Game = function(game) {
    // ...
};
Life.instance = null;
Life.Game.prototype = {
    updateData: function() {
    },

    create: function() {
        Life.instance = this;
		this._sock = new WebSocket("ws://localhost:7777/life/connect")
        this._cursors = this.game.input.keyboard.createCursorKeys();
        this._fontStyle = { 
            font: "12px Terminal", 
            fill: "#FFCC00", 
            stroke: "#333", 
            strokeThickness: 1, 
            align: "center"
        };
        
        this.game.add.sprite(0,0,'map');
        /*this._updateTimer = this.game.time.create(this.game);
        this._updateTimer.loop(1000,this.updateData,this);
        this._updateTimer.start();*/
    },

    managePause: function() {
        // ...
    },

    update: function() {
    }
};

